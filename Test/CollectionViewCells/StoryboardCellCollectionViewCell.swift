//
//  StoryboardCellCollectionViewCell.swift
//  Test
//
//  Created by Kukin Valentin on 7/10/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import UIKit

class StoryboardCellCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var label: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
    }
}
