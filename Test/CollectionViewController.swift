//
//  CollectionViewController.swift
//  Test
//
//  Created by Kukin Valentin on 7/10/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import UIKit

class CollectionViewController: UIViewController {
    
    let sectionInsets = UIEdgeInsets(top: 40.0, left: 20.0, bottom: 40.0, right: 20.0)
    
    struct Section {
        var name: String
        var numberOfItems: Int = 0
        var itemsPerRow: Int = 1
        
        mutating func changeNumberOfItems(by number: Int) {
            numberOfItems += number
            
            if numberOfItems <= 0 {
                numberOfItems = 1
            }
        }
        init(name: String) {
            self.name = name
        }
        init(name: String, numberOfItems: Int, itemsPerRow: Int) {
            self.name = name
            self.numberOfItems = numberOfItems
            self.itemsPerRow = itemsPerRow
        }
    }
    
    @IBOutlet weak var collectionView: UICollectionView?
    @IBOutlet weak var goBackButton: UIButton?
    @IBOutlet weak var widthButton: UIButton?
    @IBOutlet weak var directionButton: UIButton?
    @IBOutlet weak var addButton: UIButton?
    @IBOutlet weak var deleteButton: UIButton?
    @IBOutlet weak var refreshButton: UIButton?
    @IBOutlet weak var cityIdLabel: UILabel?
    
    private var sections: [Section] = []
    private var history: WeatherHistory?
    private var displayData: [[String]] = []
    private var weatherHistoryRequest: WeatherHistoryRequest? = WeatherHistoryRequest()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        goBackButton?.addTarget(self, action: #selector(goBackButtonClick), for: .touchUpInside)
        widthButton?.addTarget(self, action: #selector(widthButtonClick), for: .touchUpInside)
        directionButton?.addTarget(self, action: #selector(directionButtonClick), for: .touchUpInside)
        addButton?.addTarget(self, action: #selector(addButtonClick), for: .touchUpInside)
        deleteButton?.addTarget(self, action: #selector(deleteButtonClick), for: .touchUpInside)
        refreshButton?.addTarget(self, action: #selector(refreshButtonClick), for: .touchUpInside)
        
        collectionView?.allowsMultipleSelection = true
        collectionView?.dataSource = self
        collectionView?.delegate = self
        weatherHistoryRequest?.delegate = self
        
        weatherHistoryRequest?.send()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        collectionView?.collectionViewLayout.invalidateLayout()
    }
    
    private func displayLoadedData() {
        cityIdLabel?.text = "City ID: " + (history?.cityId ?? "")
        collectionView?.reloadData()
    }
  
    @objc func refreshButtonClick(_ sender: UIButton) {
        weatherHistoryRequest?.getURL = URL(string: "https://samples.openweathermap.org/data/2.5/history/city?lat=139&lon=35&appid=dfefbddd0ce87e6dbb2ee90cdfada0a6")!
        weatherHistoryRequest?.send()
    }
    
    @objc func addButtonClick(_ sender: UIButton) {
        let section = (collectionView?.numberOfSections ?? 0) - 1
        insertItem(toSection: section)
        scrollToLastItem()
    }
    
    @objc func deleteButtonClick(_ sender: UIButton) {
        let section = (collectionView?.numberOfSections ?? 0) - 1

        deleteLastItem(inSection: section)
        scrollToLastItem()
    }
    
    private func insertItem(toSection section: Int) {
        let item = (collectionView?.numberOfItems(inSection: section) ?? 0)
  
        sections[section].changeNumberOfItems(by: 1)
        collectionView?.insertItems(at: [IndexPath(item: item , section: section)])
    }
    
    private func deleteLastItem(inSection section: Int) {
        guard sections[section].numberOfItems != 1 else { return }
        let item = (collectionView?.numberOfItems(inSection: section) ?? 0) - 1
        
        sections[section].changeNumberOfItems(by: -1)
        collectionView?.deleteItems(at: [IndexPath(item: item, section: section)])
    }
    
    private func scrollToLastItem() {
        let section = (collectionView?.numberOfSections ?? 0) - 1
        let item = self.collectionView(self.collectionView!, numberOfItemsInSection: section) - 1
        let lastItemIndex = IndexPath(item: item, section: section)
        
        let layout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout
        if layout?.scrollDirection == .vertical {
            collectionView?.scrollToItem(at: lastItemIndex, at: .top, animated: true)
        } else {
            collectionView?.scrollToItem(at: lastItemIndex, at: .left, animated: true)
        }
    }
    
    @objc func widthButtonClick(_ sender: UIButton) {
        for (index, section) in sections.enumerated() {
            sections[index].itemsPerRow = (section.itemsPerRow == 3) ? 1 : 3
        }
        collectionView?.collectionViewLayout.invalidateLayout()
        
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations: {
            self.collectionView?.layoutIfNeeded()
        }, completion: nil)
    }
    
    @objc func directionButtonClick(_ sender: UIButton) {
        guard let layout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        layout.scrollDirection = (layout.scrollDirection == .vertical) ? .horizontal : .vertical

        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations: {
            self.collectionView?.layoutIfNeeded()
        }, completion: nil)
    }
    
    @objc func goBackButtonClick(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension CollectionViewController : WeatherHistoryRequestDelegate {
    
    func didLoad(history: WeatherHistory) {
        collectionView?.backgroundView = nil
        self.history = history
        displayData = history.history.map { weather in
            return [
                weather.name,
                "Clouds: \(weather.clouds)%",
                "Temp: " + String(weather.temp),
                "Speed of wind: " + String(weather.windSpeed) + " m/s"
            ]
        }
        displayLoadedData()
    }
    
    func didFail(with error: RequestErrors) {
        setErrorView(errorText: error.localizedDescription)
    }
    
    func setErrorView(errorText: String?) {
        if history != nil {
            return
        }
        let errorLabel = UILabel()
        errorLabel.text = errorText ?? "Error of loading data"
        errorLabel.textAlignment = .center
        errorLabel.backgroundColor = UIColor.init(red: 45/256, green: 0, blue: 0, alpha: 0.3)
        collectionView?.backgroundView = errorLabel
    }
}

extension CollectionViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return displayData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return displayData[section].count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellFromStoryboard", for: indexPath) as! StoryboardCellCollectionViewCell
        cell.layer.cornerRadius = 15
        cell.layer.borderWidth = 1
        cell.backgroundColor = cell.isSelected ? .orange : .cyan
        cell.label?.text = displayData[indexPath.section][indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! StoryboardCellCollectionViewCell
        cell.backgroundColor = .orange

    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! StoryboardCellCollectionViewCell
        
        cell.backgroundColor = .cyan
    }
}

extension CollectionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding = sectionInsets.left * 2
        let width = collectionView.bounds.width - padding
        let itemWidth = width
        let itemHeight = width / 3
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
}
