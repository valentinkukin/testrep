//
//  MiddleViewController.swift
//  Test
//
//  Created by Kukin Valentin on 7/4/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import UIKit

class MiddleViewController : UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var firstTextField: UITextField!
    @IBOutlet weak var secondTextField: UITextField!
    @IBOutlet weak var button: UIButton!

    override func viewDidLoad() {
        hideKeyboardWhenTappedAround()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil)
        
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardFrameChanged), name: UIResponder.keyboardDidChangeFrameNotification, object: nil);
    }
    
    func adjustInsetForKeyboardShow(_ show: Bool, notification: Notification) {
        let keyboardFrame = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let adjustmentHeight = (keyboardFrame.height) * (show ? 1 : -1)
        scrollView.contentInset.bottom = adjustmentHeight
        scrollView.scrollIndicatorInsets.bottom = adjustmentHeight
        
        print("adjustmentHeight: \(adjustmentHeight)")
    }

    @objc func keyboardWillShow(_ notification: Notification) {
        print("Keyboard will show")
        adjustInsetForKeyboardShow(true, notification: notification)
    }

    @objc func keyboardWillHide(_ notification: Notification) {
        print("Keyboard will hide")
        adjustInsetForKeyboardShow(false, notification: notification)
    }
}

extension MiddleViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField === firstTextField {
            secondTextField?.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
}

extension MiddleViewController  {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

