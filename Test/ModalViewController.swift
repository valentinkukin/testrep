//
//  ModalViewController.swift
//  Test
//
//  Created by Kukin Valentin on 7/4/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import UIKit

class ModalViewController : UIViewController {

    override func viewDidLoad() {
    }
    
    @IBAction func backButtonClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
