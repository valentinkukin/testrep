//
//  File.swift
//  Test
//
//  Created by Kukin Valentin on 7/12/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import Foundation

struct WeatherHistory {
    
    struct Weather {
        let name: String
        let clouds: Double
        let temp: Double
        let windSpeed: Double
    }
    
    let cityId: String
    let history: [Weather]
    
    init() {
        cityId = "none"
        history = []
    }
    
    init(json: Any) {
        var resultObject = json as? [String: Any]
        
        if let cityId = resultObject?["city_id"] as? Int {
            self.cityId = String(cityId)
        } else {
            self.cityId = "none"
        }
        
        var weathers: [Weather] = []
        if let list = resultObject?["list"] as? [[String: Any]] {
            for weatherInfo in list {
                var name: String?
                var clouds, temp, windSpeed: Double?
                
                if let weather = weatherInfo["weather"] as? [[String: Any]], !weather.isEmpty {
                    name = weather.first!["description"] as? String
                }
                if let cloudsInfo = weatherInfo["clouds"] as? [String: Double] {
                    clouds = cloudsInfo["all"]
                }
                if let mainInfo = weatherInfo["main"] as? [String: Any] {
                    temp = mainInfo["temp"] as? Double
                }
                if let windInfo = weatherInfo["wind"] as? [String: Any] {
                    windSpeed = windInfo["speed"] as? Double
                }
                
                if let name = name, let clouds = clouds, let temp = temp, let windSpeed = windSpeed {
                    weathers.append(Weather(name: name, clouds: clouds, temp: temp, windSpeed: windSpeed))
                }
            }
        }
        self.history = weathers
    }
}
