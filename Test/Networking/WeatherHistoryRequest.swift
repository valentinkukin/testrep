//
//  WeatherHistoryRequest.swift
//  Test
//
//  Created by Kukin Valentin on 7/13/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import Foundation

protocol WeatherHistoryRequestDelegate : class {
    func didLoad(history: WeatherHistory)
    func didFail(with error: RequestErrors)
}

class WeatherHistoryRequest {
    var getURL = URL(string: "https://1samples.openweathermap.org/data/2.5/history/city?lat=139&lon=35&appid=dfefbddd0ce87e6dbb2ee90cdfada0a6")!
    weak var delegate: WeatherHistoryRequestDelegate?
    
    func send() {
        var getRequest = URLRequest(url: getURL)
        getRequest.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: getRequest) { (data, response, error) -> Void in
            if error != nil {
                DispatchQueue.main.async {
                    self.delegate?.didFail(with: RequestErrors.serverNotFound)
                }
                return
            }
            if data != nil {
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: [])
                    let history = WeatherHistory(json: json)
                    DispatchQueue.main.async {
                        self.delegate?.didLoad(history: history)
                    }
                } catch {
                    DispatchQueue.main.async {
                        self.delegate?.didFail(with: RequestErrors.invalidData)
                    }
                }
            } else {
                DispatchQueue.main.async {
                    self.delegate?.didFail(with: RequestErrors.noResponse)
                }
            }
        }.resume()
    }
}

enum RequestErrors : LocalizedError {
    case serverNotFound
    case invalidData
    case noResponse
    
    var errorDescription: String? {
        switch self {
        case .serverNotFound: return "Error. Server not found"
        case .invalidData: return "Error. Invalid data"
        case .noResponse: return "Error. No response from server"
        }
    }
}
