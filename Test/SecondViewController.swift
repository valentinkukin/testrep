//
//  SecondViewController.swift
//  Test
//
//  Created by Kukin Valentin on 7/3/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    
    @IBOutlet weak var mainLabel: UILabel?
    
    @IBOutlet weak var button: UIButton?
    var labelText : String = ""
    
    override func viewDidLoad() {
        mainLabel?.text = labelText
        button?.addTarget(self, action: #selector(buttonClick), for: .touchUpInside)
    }
    
    @objc func buttonClick( _ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ModalVC") as? ModalViewController
        vc?.modalPresentationStyle = .pageSheet
        vc?.modalTransitionStyle = .crossDissolve
        
        present(vc!, animated: true, completion: nil)
    }
}
