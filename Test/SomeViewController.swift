//
//  SomeViewController.swift
//  Test
//
//  Created by Kukin Valentin on 7/10/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import UIKit

class SomeViewController: UIViewController {
    
    @IBOutlet weak var label: UILabel?
    @IBOutlet weak var buttonGoBack: UIButton?
    
    var labelText: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonGoBack?.addTarget(self, action: #selector(buttonGoBackClick), for: .touchUpInside)
        label?.text = labelText
    }
    
    @objc func buttonGoBackClick(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
