//
//  TabBarViewController.swift
//  Test
//
//  Created by Kukin Valentin on 7/4/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//
import  UIKit

class TabBarViewController : UITabBarController, UITabBarControllerDelegate {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self;
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        let fromView = selectedViewController?.view
        let toView = viewController.view
        
        if fromView !== toView {
            UIView.transition(from: fromView!, to: toView!, duration: 1, options: [.transitionCurlDown], completion: nil)
        }
        
        return true
    }
}
