//
//  TableViewCustomCell2.swift
//  Test
//
//  Created by Kukin Valentin on 7/9/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import UIKit

class TableViewCustomCell2: UITableViewCell {

    @IBOutlet weak var label: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        label?.text = "Some label"
        label?.frame = contentView.bounds
        //self.backgroundColor = .green
    }
}
