//
//  TestTableViewCell.swift
//  Test
//
//  Created by Kukin Valentin on 7/9/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import UIKit

class TestTableViewCell: UITableViewCell {
    
    let label = UILabel()
    var leftImageView = UIImageView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        //backgroundColor = .cyan
        setLabel()
        setImage("picture.png")
    }
    
    private func setLabel() {
        label.textAlignment = .center
        label.text = "I'am a test label"
        contentView.addSubview(label)
    }
    private func setImage(_ name: String) {
        leftImageView.image = UIImage(named: name)
        leftImageView.contentMode = .scaleAspectFit
        leftImageView.clipsToBounds = true
        contentView.addSubview(leftImageView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let parent = contentView.bounds
        label.frame = parent
        leftImageView.frame = CGRect(x: 0, y: 0, width: parent.height - 10, height: parent.height - 10)
    }
}

extension NSAttributedString {
    
    func size(constrainedTo size: CGSize) -> CGSize {
        let rect = boundingRect(with: size, options: .usesLineFragmentOrigin, context: nil)
        return CGSize(width: ceil(rect.width), height: ceil(rect.height))
    }
    
}
