//
//  NavigationControllerController.swift
//  Test
//
//  Created by Kukin Valentin on 7/4/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import UIKit

extension UITableViewCell {
    static var reuseID: String {
        return NSStringFromClass(self)
    }
}

class ThirdViewController : UIViewController {
    
    @IBOutlet weak var addCellButton: UIButton?
    @IBOutlet weak var tableView: UITableView?
    
    private var data: [String] = []
    
    struct Section {
        let name: String
        var numberOfElements = 3
        
        init(name: String) {
            self.name = name
        }
    }
    
    private var sections: [Section] = [Section(name: "Section 1") , Section(name: "Section 2"), Section(name: "Section 3")]
//    private let cells: [UITableViewCell.Type] = [
//        TableViewCustomCell.self,
//        TableViewCustomCell2.self,
//        TestTableViewCell.self
//    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addCellButton?.addTarget(self, action: #selector(addCellButtonClick), for: .touchUpInside)
        tableView?.register(UITableViewCell.self, forCellReuseIdentifier: UITableViewCell.reuseID)
        tableView?.register(TestTableViewCell.self, forCellReuseIdentifier: TestTableViewCell.reuseID)
        tableView?.dataSource = self
        tableView?.delegate = self
    }
    
    @objc func addCellButtonClick(_ sender: UIButton) {
        sections.append(Section(name: "Section \(sections.count + 1)"))
        tableView?.reloadData()
    }
}

extension ThirdViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].numberOfElements
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section].name
    }
    
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return "Footer of " + sections[section].name
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row % 3 {
        case 0: return tableView.dequeueReusableCell(withIdentifier: "customCell1", for: indexPath)
        case 1: return tableView.dequeueReusableCell(withIdentifier: "customCell2", for: indexPath)
        case 3: return tableView.dequeueReusableCell(withIdentifier: TestTableViewCell.reuseID, for: indexPath) as! TestTableViewCell
        default:
            return tableView.dequeueReusableCell(withIdentifier: TestTableViewCell.reuseID, for: indexPath) as! TestTableViewCell
        }
//        return tableView.dequeueReusableCell(withIdentifier: cells[indexPath.row % 3].reuseID, for: indexPath)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            sections[indexPath.section].numberOfElements -= 1
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
}

extension ThirdViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SomeVC") as? SomeViewController
//        vc?.modalTransitionStyle = .flipHorizontal
//        vc?.modalPresentationStyle = .fullScreen
//        vc?.labelText = "Hello from " + String(indexPath.row % 3 + 1) + " cell of " + String(indexPath.section + 1) + " section"
//
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CollectionVC") as? CollectionViewController
        vc?.modalTransitionStyle = .flipHorizontal
        vc?.modalPresentationStyle = .fullScreen
        if let vc = vc {
            present(vc, animated: true, completion: nil)
        }
    }
}
