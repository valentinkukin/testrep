//
//  ViewController.swift
//  Test
//
//  Created by Kukin Valentin on 7/2/18.
//  Copyright © 2018 Kukin Valentin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {


    @IBOutlet weak var stackVerticalConstraint: NSLayoutConstraint?
    
    @IBOutlet weak var attributedLabel: UILabel?
    @IBOutlet weak var firstTextField: UITextField?
    @IBOutlet weak var secondTextField: UITextField?
    @IBOutlet weak var button: UIButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardFrameChanged), name: UIResponder.keyboardDidChangeFrameNotification, object: nil);
        
        button?.isEnabled = false
        button?.addTarget(self, action: #selector(heiButtonClick), for: .touchUpInside)
        let attributedString = NSAttributedString(string: "Attributed string", attributes: [.backgroundColor: UIColor.yellow])
        attributedLabel?.attributedText = attributedString
        navigationController?.navigationBar.titleTextAttributes = [.kern: 10]
        navigationItem.title = "Hello world"
        
        let view = UIView()
        let imageView = UIImageView(image: UIImage(named: "picture.png"))
        imageView.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.center = view.center
        view.addSubview(imageView)
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: view)

        //tabBarItem.title = "@"
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidChangeFrameNotification, object: nil)
    }
    
    @objc func keyboardFrameChanged(notification: Notification) {
        guard
            let button = button,
            let userInfo = notification.userInfo,
            let beginFrame = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue,
            let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
                return
        }

        let d = endFrame.minY - beginFrame.minY
        if d < 0 {
            stackVerticalConstraint?.constant = min(0, endFrame.minY - button.frame.maxY) - 3// - endFrame.minY)
        }
        if d > 0 {
            stackVerticalConstraint?.constant = 0
        }
        view.setNeedsLayout()
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    
    @objc func heiButtonClick( _ sender: UIButton) {
        let secondVC = storyboard?.instantiateViewController(withIdentifier: "SecondVC") as! SecondViewController
        
        let text1 = firstTextField?.text ?? ""
        let text2 = secondTextField?.text ?? ""
        secondVC.labelText = text1 + " " + text2
        
        self.navigationController?.pushViewController(secondVC, animated: true)
    }
    
    @IBAction func firstTextFieldEditChanged(_ sender: Any) {
        toggleButton()
    }
    
    @IBAction func secondTextFieldEditChanged(_ sender: Any) {
        toggleButton()
    }
    
    func toggleButton() {
        let firstCount = firstTextField?.text?.count ?? 0
        let lastCount = secondTextField?.text?.count ?? 0
        button?.isEnabled = firstCount >= 3 && lastCount >= 8
    }
    
}

extension ViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.backgroundColor = UIColor.lightGray
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.backgroundColor = UIColor.white
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField === firstTextField {
            secondTextField?.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
}

extension ViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
